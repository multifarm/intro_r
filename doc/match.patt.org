#+TITLE: Matching patterns introduction using R: grep, gsub, regex
#+DATE: 2022-02-10
#+AUTHOR: Jose
#+OPTIONS: num:nil toc:2
#+PROPERTY: header-args:R :results output :session *R1* :cmdline :tangle yes
#+PROPERTY: header-args:R+ :exports both

#+begin_src R
  x = c(40, 30, 20, 44, 54)
#+end_src

#+RESULTS:

#+begin_src R
  y = paste0(x, "L")
  y
#+end_src

#+RESULTS:
: [1] "40L" "30L" "20L" "44L" "54L"

** gsub function

Remove the first component of each string

#+begin_src R
  gsub("^.{1}", "", y)
#+end_src

#+RESULTS:
: [1] "0L" "0L" "0L" "4L" "4L"

Remove the last component of each string

#+begin_src R
  gsub(".{1}$", "", y)
#+end_src

#+RESULTS:
: [1] "40" "30" "20" "44" "54"

#+begin_src R
sub(".{1}$", "", y)
#+end_src

#+RESULTS:
: [1] "40" "30" "20" "44" "54"

#+begin_src R
  noquote(y)
#+end_src

#+RESULTS:
: [1] 40L 30L 20L 44L 54L

