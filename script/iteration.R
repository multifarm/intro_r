#' ---
#' title: "Iteration in R"
#' date: "2021-12-04"
#' author: "Jose https://ajuda.multifarm.top"
#' output:
#'   html_document:
#'    code_folding: show
#'    toc: yes
#'    toc_float:
#'      smooth_scroll: true
#'    df_print: paged
#'    highlight: zenburn
#' ---

#' remove objects
#'
objects()

rm(list = ls())

#' data

data(sleep)

str(sleep)

#' ## for loop
#'
#' for( var in seq ) expr
#' Where 'var' us a name and 'seq' is a vector and 'expr' an expression

for(j in names(sleep)) print(class(names(sleep)))

sapply(sleep, class)

lapply(sleep, class)

## The language has other iteration operators (while() and repeat), and
## the usual conditional operators (if ... else).

#' Check package 'SoDA'

#' ## References
#'
#' John M. Chambers. Software for Data Analysis: Programming with R.
## @book{chambers2008software,
##   title={Software for data analysis: programming with R},
##   author={Chambers, John M},
##   volume={2},
##   year={2008},
##   publisher={Springer}
## }
